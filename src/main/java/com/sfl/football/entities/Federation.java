/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.entities;

/**
 *
 * @author steph
 */
public class Federation {
    private String name;
    private String pays;
    private int nbLicencies;
    
    public Federation(){}
    
    public Federation(String aName, String aPays, int aNbLicencies){
        name = aName;
        pays = aPays;
        nbLicencies = aNbLicencies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public int getNbLicencies() {
        return nbLicencies;
    }

    public void setNbLicencies(int nbLicencies) {
        this.nbLicencies = nbLicencies;
    }
}
