/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.exception;

/**
 *
 * @author steph
 */
public class CountryException extends Exception {
    public CountryException(){
        super();
    }
    public CountryException(String message){
        super(message);
    }
}
