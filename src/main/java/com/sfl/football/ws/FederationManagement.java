/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.ws;

import com.sfl.football.entities.Federation;
import com.sfl.football.exception.CountryException;
import com.sfl.football.service.FederationService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author steph
 */
@WebService
public class FederationManagement {
    
    @Inject
    FederationService federationService;
    
    @WebMethod
    public String getTitle(){
        return "FIFA";
    }
    
    @WebMethod(operationName = "federationParPays")
    public Federation getFederationByCountry(@WebParam(name = "pays") String country) 
            throws CountryException {
        if(country.equalsIgnoreCase("toto")){
            throw new IllegalStateException("Test d'une RuntimeException...");
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(FederationManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return federationService.getFederationByCountry(country);
    }
}
