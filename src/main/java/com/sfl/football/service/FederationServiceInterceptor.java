/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.InvocationContext;

/**
 *
 * @author steph
 */
public class FederationServiceInterceptor {
    
    static final Logger logger = Logger.getLogger(FederationServiceInterceptor.class.getName());
    
    public FederationServiceInterceptor(){}
    
    //@AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {
        logger.log(Level.INFO, "FederationServiceInterceptor intercepte (log this) et proceed...");
        return context.proceed();
    }
}
