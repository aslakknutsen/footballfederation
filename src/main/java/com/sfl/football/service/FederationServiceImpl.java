/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.service;

import com.sfl.football.entities.Federation;
import com.sfl.football.exception.CountryException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author steph
 */
@Stateless
//@Interceptors(FederationServiceInterceptor.class)
public class FederationServiceImpl implements FederationService {

    private List<Federation> federations;

    public FederationServiceImpl() {
        federations = new ArrayList<Federation>();
        federations.add(new Federation("FFF", "FRANCE", 3500000));
        federations.add(new Federation("FIF", "ITALIE", 2500000));
        federations.add(new Federation("FEF", "ESPAGNE", 3000000));
        federations.add(new Federation("FAF", "ALLEMAGNE", 4500000));
    }

    @Override
    public Federation getFederationByCountry(String country) 
            throws CountryException {
        if(null == country){
            throw new CountryException("Pays obligatoire!");
        }
        for (Federation federation : federations) {
            if(federation.getPays().equals(country)){
                return federation;
            }
        }
        
        throw new CountryException("Pays inconnu");
    }
}
