/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.service;

import com.sfl.football.entities.Federation;
import com.sfl.football.exception.CountryException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.xml.ws.WebServiceException;

/**
 *
 * @author steph
 */
@Decorator
public class FederationServiceDecorator implements FederationService{
    
    @Inject
    @Delegate
    @Any
    FederationService federationService;

    @Override
    public Federation getFederationByCountry(String country) throws CountryException {
        Federation federation = null;
        Logger.getLogger("FederationServiceDecorator").log(Level.INFO, "LE DECORATOR EST BIEN APPELé...");
        try{
            federation = federationService.getFederationByCountry(country);
        }
        catch(CountryException ce){
            System.err.println("Mon decorator peut gérer une countryException ("+ce.getMessage()+")...");
            throw ce;
        }
        catch(WebServiceException wse){
            System.err.println("Mon decorator peut gérer ici l'exception [" + wse.getMessage() +"] levée par l'appel au Service Web.");
            System.err.println("Typiquement, si j'ai une erreur de timeout, j'envoie un message dans une fil MQ...");
            throw wse;
        }
        
        return federation;
    }
    
}
