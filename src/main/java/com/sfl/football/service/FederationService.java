/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.service;

import com.sfl.football.entities.Federation;
import com.sfl.football.exception.CountryException;

/**
 *
 * @author steph
 */
public interface FederationService {

    public Federation getFederationByCountry(String country) 
            throws CountryException;
}
