/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sfl.football.service;

import com.sfl.football.entities.Federation;
import com.sfl.football.exception.CountryException;
import com.sfl.football.ws.FederationManagement;
import java.io.File;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;

/**
 *
 * @author steph
 */
//@RunWith(Arquillian.class)
public class FederationServiceTest {
    
    public static WebArchive war;
    
    //@Deployment
    public static WebArchive createArchive(){
        war = ShrinkWrap.create(WebArchive.class);
        war.addPackage(FederationService.class.getPackage());
        war.addPackage(Federation.class.getPackage());
        war.addPackage(CountryException.class.getPackage());
        war.addPackage(FederationManagement.class.getPackage());
        try{
            war.addAsWebInfResource(new File("src/main/webapp/WEB-INF/beans.xml"));
        }catch(Exception e){
            e.printStackTrace(System.err);
        }
        
        return war;
    }
    
    //@Test
    public void simpleTest(){
        
        FederationManagement fed = new FederationManagement();
        Federation resultat = null;
        try {
            System.err.println(war.toString(true));
            resultat = fed.getFederationByCountry("FRANCE");
        } catch (CountryException countryEx) {
            Assert.fail("FRANCE devrait exister!");
        }catch(Exception ex){
            ex.printStackTrace(System.err);
            Assert.fail("Pas prévue!.."+ex);
        }
        Assert.assertEquals("FRANCE", resultat.getName());
    }
}
